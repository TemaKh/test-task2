import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReadAndPrint {
    public static void main(String[] args) {
        String path = ".\\resources\\testfile1.txt";
        String string = null;
        try {
            string = new String(Files.readAllBytes(Paths.get(path)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Integer> list = new ArrayList<>();

        for (String element : string.split(",")) {
            list.add(Integer.parseInt(element));
        }

        Collections.sort(list);
        System.out.println("Значения по возрастанию: " + list);

        Collections.reverse(list);
        System.out.println("Значения по убыванию: " + list);
    }
}
